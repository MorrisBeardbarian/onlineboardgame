global.DEBUG = true;

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var session = require('express-session');
var sharedsession = require("express-socket.io-session");
var fs = require('fs');
var ejs = require('ejs');

//Router routes autoloader
var gameRouter = require('./routes/game.route');
var commonRouter = require('./routes/common.route');
var apiRouter = require('./routes/api.route');

//App globals setup
global.version = '1.0.0'

var port = 8080;
if(global.DEBUG)
	port = 7171;

var app = express();
app.version = global.version;
global.gameID = 0;

global.hostname = "https://browserBoardgameLDJam46.com/";
if(global.DEBUG)
	global.hostname = "http://localhost:"+port+"/";

global.Buffer = global.Buffer || require('buffer').Buffer;

//Sqlite
// global.db = new sqlite3.Database('./users.db', (err) => {
// 	if (err)
//     	console.error(err.message);
// 	else
// 		console.log('Connected to the users database.');
// });

//Missing methods
if (typeof btoa === 'undefined') {
	global.btoa = function (str) {
		return new Buffer(str, 'binary').toString('base64');
	};
}

if (typeof atob === 'undefined') {
	global.atob = function (b64Encoded) {
		return new Buffer(b64Encoded, 'base64').toString('binary');
	};
}

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Session
app.set('trust proxy', 1) // trust first proxy
var sessionMiddleware = session({
	secret: 'DnEyDktAr3xLnVc',
	resave: false,
	saveUninitialized: true,
	cookie: { secure: false }
});
app.use(sessionMiddleware);

// Set Static Folder
app.use('/public', express.static('public'));

// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Game Handlers
global.users_connected = {
	//username: player_obj
};

global.lobbies = {
	//lobby_id: game_obj
}

// Router
gameRouter.routeMe(app);
commonRouter.routeMe(app);
apiRouter.routeMe(app);

// Server star
//var serv = require('https').Server(options, app);
var serv = require('http').Server(app);

// Socket IO
var io = require('socket.io')(serv); //{path: '/joc/socket.io'}

io.use(sharedsession(sessionMiddleware, {
    autoSave:true
}));

//App Custom Modules
var Faction = require('./modules/faction');
var Resource = require('./modules/resource');
var Card = require('./modules/card');
var Dice = require('./modules/dice');
var Game = require('./modules/game');
var my_cards = require('./modules/card_list');
var Player = require('./modules/player');

var get_dice_throw = function(){
	return Math.round(Math.random()*5) + 1; // (from 1 -> 6)
}

io.on('connection', function(socket){

	//On User Connect
	socket.on('user_connected', function(data){
		console.log(global.users_connected[socket.handshake.session.username] );
		if(socket.handshake.session.username != undefined && global.users_connected[socket.handshake.session.username] == undefined){
			//User not connected, we connect him now
			global.users_connected[socket.handshake.session.username] = new Player(socket.id, socket.handshake.session.username);
			socket.emit("update", {current_screen: "lobbySearch", lobbies: global.lobbies, you: socket.handshake.session.username});
		}else{
			//User already connected, he shouldn't be able to connect twice, so we show him an error message
			console.log("User tried to connect again");
			socket.emit("already_connected");
			socket.disconnect();
		}
		//Check login status
		//Set him up for data sending
		//send player data for update screen (lobby_finding)
	});

	//Player disconnected
	socket.on('disconnect', function(data){
		//Check if player was in a game
		if(socket.handshake.session.username && global.users_connected[socket.handshake.session.username].is_in_game()){
			//End game
		}else{
			//Do nothing
		}
	});

	socket.on('lobby_creation', function(data){
		//check login status
		if(socket.handshake.session.username != undefined && global.users_connected[socket.handshake.session.username] != undefined){
			//Check if player is already in a lobby
			if(!global.users_connected[socket.handshake.session.username].is_in_game()){
				//Resources -> Name, starting value, win condition value, lose condition value
				let resource1 = new Resource("Manpower", 2, 10, 0);
				let resource2 = new Resource("Money", 10, false, false);
				let resource3 = new Resource("Intelligence", 1, false, false);
				let resource4 = new Resource("Military", 10, false, 0);
				let resource5 = new Resource("Influence", 10, false, 0);
				let resource6 = new Resource("Intelligence", 1, false, false);
				//Factions -> Name, list of resources, maximum players
				let faction1 = new Faction("Revolutionaries", [resource1, resource2, resource3], 3);
				let faction2 = new Faction("Regime", [resource4, resource5, resource6], 3);
				//Game
				let new_lobby = new Game([faction1, faction2], global.users_connected[socket.handshake.session.username]);
			}else{
				socket.emit("error", {error: "You are already in a lobby"});
			}
		}
		//Create lobby with player as owner
		//Set up factions which players can join
		//place player in neither faction
		//Send player data for update screen (lobby_screen)
	});

	socket.on('player_join_lobby', function(data){
		//check login status
		if(socket.handshake.session.username != undefined && global.users_connected[socket.handshake.session.username] != undefined){
			//check if player is already in a lobby
			if(!global.users_connected[socket.handshake.session.username].is_in_game()){
				//add player to lobby
				//send lobby players data for update screen
				//send player data for update screen (lobby_screen)
			}else{
				socket.emit("error", {error: "You are already in a lobby"});
			}
		}
	});

	socket.on('player_exit_lobby', function(data){
		//check login status
		//check if player is in lobby
		//send lobby players data for update screen
		//send player data for update screen (lobby_finding)
	});

	socket.on('lobby_start_game', function(data){
		//check login status
		//check if player is owner of a lobby
		//check lobby if start is possible
		//set lobby as started
		//give players their roles

		//start game
	});

	socket.on('do_action', function(data){
		//check login status
		//check if player is in ongoing game
		//check if it's player's turn
		//set action as to be done (if action requires extra decision)
		//send acknowledgement to player
	});

	socket.on('draw_card', function(data){

	});

	socket.on('throw_dice', function(data){

	})
});

serv.listen(port);