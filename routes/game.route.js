var express = require('express');
var router = express.Router();

function routeMe(app){
	router.get('/', function(req, res, next){
		//res.send({version:app.version});
		res.redirect('/game/play');
	});
	app.use('/game', router);
	
	var normalizedPath = require("path").join(__dirname, "gameRoutes");
	require("fs").readdirSync(normalizedPath).forEach(function(file) {
		var newRoute = require("./gameRoutes/" + file);
		app.use('/game', newRoute);
	});
};



module.exports.routeMe = routeMe;