var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var request = require('request');
var session = require('express-session');

var Player = require('../../modules/player');

var app = express();

app.use(bodyParser.urlencoded({extended: true}));

router.get('/login', function(req, res, next){
	redirected = 0;
	if(req.session.user_id !== undefined && req.query.room == undefined){
		redirected = 1;
		res.redirect('/game/play');
	}
	if(redirected == 0){
		res.render('login.ejs', {
			errors: []
		});
	}
});

router.post('/login', function(req, res, next){
	_errors = [];
	redirected = 0;

	if(req.body.username == "")
		_errors.push("You must write the username you want to use");
	
	_username = req.body.username;
	if(_username.length > 16 || _username.length < 1)
		_errors.push("Your username must have between 1 and 16 characters");

	if(global.users_connected[_username])
		_errors.push("Username already in use. Please try another username");

	if(_errors.length == 0){
		//var userObj = new Player(_username);
		//global.users_connected[_username] = userObj;
		req.session.username = _username;
		res.redirect('/game/play');
	}else{
		res.render('login.ejs', {
			errors: _errors
		});
	}
});

module.exports = router;