var express = require('express');
var router = express.Router();
var session = require('express-session');

function routeMe(app){
	// router.get('/', function(req, res, next){
	// 	res.send({version:app.version});
	// });
	app.use('/api', router);
	
	var normalizedPath = require("path").join(__dirname, "apiRoutes");
	require("fs").readdirSync(normalizedPath).forEach(function(file) {
		var newRoute = require("./apiRoutes/" + file);
		app.use('/api', newRoute);
	});
};



module.exports.routeMe = routeMe;