var express = require('express');
var router = express.Router();
var session = require('express-session');

function routeMe(app){
	// router.get('/', function(req, res, next){
	// 	//Redirect to login page if not logged in otherwise on game list page
	// 	if(req.session.user_id == undefined)
	// 		res.redirect('http://localhost:7171/login');
	// 	else
	// 		res.redirect('http://localhost:7171/play');
	// });
	
	app.use('/', router);
	
	var normalizedPath = require("path").join(__dirname, "commonRoutes");
	require("fs").readdirSync(normalizedPath).forEach(function(file) {
		var newRoute = require("./commonRoutes/" + file);
		app.use('/', newRoute);
	});
};

module.exports.routeMe = routeMe;