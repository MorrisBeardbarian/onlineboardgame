var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var request = require('request');
var session = require('express-session');

var app = express();

app.use(bodyParser.urlencoded({extended: true}));

router.get('/play', function(req, res, next){
	if(req.session.username !== undefined){
		res.render('main.ejs', {
			hostname_var: global.hostname,
			errors: []
		});
	}else{
		res.redirect('/login');
	}
});

module.exports = router;