ratio=16/9
//Get width
var _width = window.innerWidth;

//Calculate height
var _height = _width * ratio;

//Recalculate dimensions if we got them wrong the first time
if(_height > window.innerHeight){
	_height = window.innerHeight;
	_width = _height * ratio;
}

class Boardgame extends Phaser.Scene {

	constructor(){
		super({
			key: 'onlineBoardgame'
		})
	}

	preload() {
		this.load.scenePlugin({
			key: 'rexuiplugin',
			url: "/public/js/rexui.js",
			sceneKey: 'rexUI'
		});
		//this.
		//this.load.scenePlugin('rexuiplugin', 'https://raw.githubusercontent.com/rexrainbow/phaser3-rex-notes/master/dist/rexuiplugin.min.js', 'rexUI', 'rexUI');
	}

	create() {
		//this.
	}

	completeUI(data){
		if(data.current_screen == "lobbySearch"){

		}else if(data.current_screen == "lobby"){

		}else if(data.current_screen == "game"){
			let game_data = data.get_game_data();
			let factions = data.get_factions();
			let faction1_name = factions[0].get_name();
			let faction2_name = factions[1].get_name();

			let factions1_players = factions[0].get_players();
			let factions2_players = factions[1].get_players();

			let factions1_resources = factions[0].get_resources();
			let factions2_resources = factions[1].get_resources();
		}
	}
}

const config = {
	type: Phaser.AUTO,
	parent: "gameCanvas",
	width: _width,
	height: _height,
	scene: Boardgame,
	plugins: {
	},
	scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
};

const game = new Phaser.Game(config);