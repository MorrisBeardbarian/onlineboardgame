module.exports = function(){
	return [
		new Card("Christmas comes with presents", false, function(game){
			game.get_faction("Revolutionaries").add_to_resource("Money", 3, false, false);
			game.get_faction("Regime").add_to_resource("Intelligence", 2, false, false);
		}),
		new Card("War started with another country", false, function(game){
			game.get_faction("Revolutionaries").add_to_resource("Manpower", -2, false, false);
			game.get_faction("Regime").add_to_resource("Military", -2, false, false);
		})
	];
}