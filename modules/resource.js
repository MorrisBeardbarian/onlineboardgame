module.exports = class ResourceObj{
	constructor(name, starting_value, win_condition, lose_condition){
		this.name = name;
		this.value = starting_value
		this.win_condition = win_condition;
		this.lose_condition = lose_condition;
	}

	check_state(){
		if(this.value >= win_condition)
			return 1;

		if(this.value <= lose_condition)
			return -1;

		return 0;
	}

	get_name(){
		return this.name;
	}

	get_value(){
		return this.value;
	}

	add_to_value(add_value, should_lose = true, should_win = true){
		this.value += add_value;
		if(!should_lose && this.value <= this.lose_condition){
			this.value += this.lose_condition - this.value + 1;
		}
		if(!should_win && this.value >= this.win_condition){
			this.value += this.win_condition - this.value - 1;
		}
	}
}