module.exports = class GameObj{

	constructor(available_factions, owner_player){
		this.id = global.gameID;
		global.gameID=global.gameID+1;
		this.gameVersion = global.version;
		this.factions = {};
		this.faction_names = [];
		for(let i=0; i<available_factions.length; i++){
			this.faction_names.push(available_factions[i].get_name());
			this.factions[available_factions[i].get_name()] = available_factions[i];
		}
		this.lobbyPlayers = [];
		this.lobbyPlayers.push(owner_player);
		this.started = false;
		this.current_faction = 0;
	}

	get_maximum_players(){
		let max_players = 0;
		let factions_keys = Object.keys(this.factions);
		for(let i = 0; i<factions_keys.length; i++){
			max_players += this.factions[factions_keys[i]].get_max_players();
		}
	}

	get_factions(){
		return this.factions;
	}

	add_player(player){
		this.lobbyPlayers.push(player);
	}

	player_change_faction(player_username, faction_number){
		let player = undefined;
		for(let i = 0; i<lobbyPlayers.length; i++){
			if(lobbyPlayers[i].get_name() == player_username){
				player = lobbyPlayers[i];
			}
		}
		if(player){
			let faction_name = this.faction_names[faction_number];
			if(player.in_faction()){
				player.in_faction().remove_player(player_username);
			}
			player.set_faction(this.factions[faction_name]);
			this.factions[faction_name].add_player(player);
		}else{
			console.log("Player could not be found, aborting player_change_faction call");
		}
	}

	get_faction(name){
		return this.factions[name];
	}

	next_turn(){
		let previous_faction = this.current_faction;
		this.current_faction = (this.current_faction + 1) % Object.keys(this.factions).length;
	}

	get_game_ID(){

	}

	get_game_players(){

	}

	get_game_player_data(){

	}

	check_game_state(){
		let factions_keys = Object.keys(this.factions);
		for(let i = 0; i<factions_keys.length; i++){
			if(this.factions[factions_keys[i]] == 1){
				return 1;
			}
			if(this.factions[factions_keys[i]] == -1){
				return -1;
			}
		}
		return 0;
	}
}