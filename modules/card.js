module.exports = class CardObj{
	constructor(card_text, card_image = false, card_action = function(game){console.log("null");}){

	}

	/**
	* Best shuffling algorithm = Fisher–Yates Shuffle. O(n) in-place
	*/
	shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}
}