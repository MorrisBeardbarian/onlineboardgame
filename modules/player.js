module.exports = class PlayerObj{

	constructor(ioSocketID, username){
		this.ioSocketID = ioSocketID;
		this.username = username;
		this.in_game = false;
		this.game = undefined;
		this.faction = undefined;
	}

	is_in_game(){
		return this.in_game;
	}

	set_game(game_id){
		this.game = game_id;
		this.in_game = true;
	}

	disconnect_from_game(){
		this.game = undefined;
		this.in_game = false;
	}

	in_faction(){
		return this.faction;
	}

	set_faction(faction){
		this.faction = faction;
	}

	remove_from_faction(){
		this.faction = undefined;
	}

}