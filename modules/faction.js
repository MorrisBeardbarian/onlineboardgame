module.exports = class FactionObj{
	constructor(name, available_resources, max_players = 3){
		this.name = name;
		this.resources = {};
		for(let i=0; i<available_resources.length; i++){
			this.resources[available_resources[i].get_name()] = available_resources[i];
		}
		this.players = [];
		this.current_player = 0;
		this.max_players = max_players;
	}

	next_player(){
		this.current_player = (this.current_player + 1) % this.players.length;
	}

	get_name(){
		return this.name;
	}

	check_state(){
		let resource_keys = Object.keys(this.resources);
		for(let i=0; i<resource_keys.length; i++){
			if(this.resources[resource_keys[i]].check_state() == 1){
				return 1;
			}
			if(this.resources[resource_keys[i]].check_state() == -1){
				return -1
			}
		}
		return 0;
	}

	add_to_resource(resource_name, value_to_add, should_lose = true, should_win = true){
		this.resources[resource_name].add_to_value(value_to_add, should_lose, should_win);
	}

	get_max_players(){
		return this.max_players;
	}
}